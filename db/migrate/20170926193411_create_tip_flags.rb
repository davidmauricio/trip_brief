class CreateTipFlags < ActiveRecord::Migration[5.1]
  def change
    create_table :tip_flags do |t|
      t.references :tip, foreign_key: true
      t.references :user, foreign_key: true
      t.integer :status, default: TipFlag::Status[:active]

      t.timestamps
    end
  end
end

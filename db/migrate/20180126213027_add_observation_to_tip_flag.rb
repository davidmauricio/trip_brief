class AddObservationToTipFlag < ActiveRecord::Migration[5.1]
  def change
  	add_column  :tip_flags, :observation, :string
  end
end

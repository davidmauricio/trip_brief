class AddVisitsCountToTips < ActiveRecord::Migration[5.1]
  def change
    add_column :tips, :visits_count, :integer, default: 0
  end
end

class CreateCategories < ActiveRecord::Migration[5.1]
  def change
    create_table :categories do |t|
      t.string :name
      t.integer :status, default: Category::Status[:active]

      t.timestamps
    end
  end
end

class CreateTips < ActiveRecord::Migration[5.1]
  def change
    create_table :tips do |t|
      t.string :subject
      t.text :body
      t.boolean :is_real_time, default: false
      t.date :expiry_date
      t.text :month_applicable
      t.integer :status, default: Tip::Status[:active]
      t.references :user, foreign_key: true
      t.integer :entity_type, default: Tip::EntityTypes[:country]
      t.references :country, foreign_key: true
      t.references :city, foreign_key: true
      t.references :attraction, foreign_key: true

      t.timestamps
    end
  end
end

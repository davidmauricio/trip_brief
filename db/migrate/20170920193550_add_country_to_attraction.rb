class AddCountryToAttraction < ActiveRecord::Migration[5.1]
  def change
    add_reference :attractions, :country, foreign_key: true
  end
end

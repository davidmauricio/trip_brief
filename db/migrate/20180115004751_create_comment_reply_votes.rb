class CreateCommentReplyVotes < ActiveRecord::Migration[5.1]
  def change
    create_table :comment_reply_votes do |t|

		t.references :tip, foreign_key: true
		t.references :user, foreign_key: true
		t.references :comment, foreign_key: true
		t.references :comment_replay, foreign_key: true
		
      t.timestamps
    end
  end
end

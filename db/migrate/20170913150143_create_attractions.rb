class CreateAttractions < ActiveRecord::Migration[5.1]
  def change
    create_table :attractions do |t|
      t.string :name
      t.references :city, foreign_key: true
      t.integer :status, default: Attraction::Status[:active]

      t.timestamps
    end
  end
end

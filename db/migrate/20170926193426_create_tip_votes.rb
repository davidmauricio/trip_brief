class CreateTipVotes < ActiveRecord::Migration[5.1]
  def change
    create_table :tip_votes do |t|
      t.references :tip, foreign_key: true
      t.references :user, foreign_key: true

      t.timestamps
    end
  end
end

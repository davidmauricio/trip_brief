class AddUserColumnsToUsers < ActiveRecord::Migration[5.1]
	def change
		add_column :users, :username, :string
		add_column :users, :status, :integer, default: User::Status[:active]
		add_column :users, :birth_year, :integer
		add_column :users, :gender, :integer, default: User::Genders[:male]
		add_column :users, :role, :integer, default: User::Roles[:normal]
		add_column :users, :bio, :text
		add_column :users, :recieve_email_updates, :boolean
		add_column :users, :external_link, :string
	end
end

class AddSlugToAttraction < ActiveRecord::Migration[5.1]
  def change
    add_column :attractions, :slug, :string
  end
end

Rails.application.routes.draw do
    mount Ckeditor::Engine => '/ckeditor'
    root 'home#index'
    devise_for :users, controllers: { omniauth_callbacks: 'users/omniauth_callbacks' }

    get '/welcome/index' => 'welcome#index', as: 'index_welcome'

    get '/my_profile' => 'profile#show', as: 'my_profile'
    get '/my_profile/index' => 'profile#index', as: 'index_profile'
    get '/my_profile/edit' => 'profile#edit', as: 'edit_profile'
    post '/my_profile/edit' => 'profile#update', as: 'update_profile'
    get '/destinations/details' => 'destinations#details',as: 'destinations_details'
    # Destinations

    get '/destinations' => 'destinations#index', as: 'destinations'
    get '/destinations/search' => 'destinations#search'
    get '/destinations/:type/:id' => 'destinations#show', as: 'destination'
    post '/destinations' => 'destinations#create'

    #Bookmarks
    resources :favorite_tips, only: [:create, :destroy]
    
    get '/bookmarks' => 'bookmarks#index', as: 'bookmarks'

    namespace :admin do
        get '/' => 'home#index'
        resources :users
        resources :countries
        resources :cities
        resources :attractions
        resources :categories
        resources :tips, only: [:index, :edit, :update, :destroy, :show]
        resources :tip_flags, only: [:index, :show, :destroy]
    end

    get '/d/:country_name_or_attraction_name' => 'destinations#show', as: 'country_or_attraction'
    get '/d/:country_name_or_attraction_name/tag/*tags' => 'destinations#show'
    get '/d/:country_name/:city_or_attraction_name' => 'destinations#show', as: 'city_or_attraction_name'
    get '/d/:country_name/:city_or_attraction_name/tag/*tags' => 'destinations#show'
    get '/d/:country_name/:city_name/:attraction_name' => 'destinations#show', as: 'attraction_name'
    get '/d/:country_name/:city_name/:attraction_name/tag/*tags' => 'destinations#show'

    get '/u/:username' => 'users#show', as: 'user_profile'

    resources :tips do
        resources :comments
        
        collection do
            get 'destination_search_autocomplete'
        end
        
        member do
            get 'body'
            post 'flag'
            post 'vote'
        end
    end
    post '/comments/vote_comment_reply' => 'comments#vote_comment_reply', as: 'add_vote_comment_reply'
    post '/comments/vote_comment' => 'comments#vote_comment', as: 'add_vote_comment'
    post '/comments/create_comment_reply' => 'comments#create_comment_reply', as: 'create_comment_reply'
    post '/tips/flag_tip' => 'tips#create_flag_tip', as: 'add_flag_tip'
    post '/tips/vote_tip' => 'tips#vote_tip', as: 'add_vote_tip'
    post '/tips/favorite_tip' => 'tips#favorite_tip', as: 'add_favorite_tip'
    get '/tips/detail' => 'tips#detail', as: 'details_tips'

    get '/d/:country_name/:city_or_attraction_name/new_tip' => 'destinations#new_tip'
    get '/d/:country_name/:city_name/:attraction_name/new_tip' => 'destinations#new_tip'

    get '/about' => 'home#about'
    get '/terms' => 'home#terms'
    get '/privacy' => 'home#privacy'
    get '/careers' => 'home#careers'


    get '/api/countries' => 'home#countries', as: 'api_get_countries'
    get '/api/cities' => 'home#cities', as: 'api_get_cities'

    
    
end

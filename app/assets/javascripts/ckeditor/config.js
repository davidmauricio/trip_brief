CKEDITOR.editorConfig = function( config ) {

  config.toolbar = 'Full';

  config.toolbar_Full =
  [
  
  { name: 'basicstyles', items : [ 'Bold','Italic','Underline','-','RemoveFormat' ] },
  { name: 'editing', items : [ 'Find','Replace','-','SelectAll','-','SpellChecker', 'Scayt' ] },
  { name: 'forms', items : [ 'Form', 'Checkbox', 'Radio', 'TextField', 'Textarea', 'Select', 'Button', 'ImageButton', 'HiddenField' ] },
  { name: 'paragraph', items : [ 'NumberedList','BulletedList','-','-','Blockquote','CreateDiv','-','JustifyLeft','JustifyCenter','JustifyRight','JustifyBlock','-','BidiLtr','BidiRtl' ] },
  { name: 'links', items : [ 'Link','Unlink' ] },
  { name: 'insert', items : [ 'Image','Flash','Table','HorizontalRule','Smiley','SpecialChar','PageBreak' ] },
  { name: 'styles', items : [ 'Styles','Format','Font','FontSize' ] },
  { name: 'colors', items : [ 'TextColor','BGColor' ] },
  { name: 'tools', items : [ 'Maximize', 'ShowBlocks','-','About' ] },
  ];
  
    config.removeButtons = 'Outdent,Indent,Source,Strike,Subscript,Superscript,CopyFormatting,Styles,Language,Save,NewPage,DocProps,Preview,Print,Templates,document,PasteText,PasteFromWord,Find,Replace,SelectAll,Scayt,Form,Checkbox,Radio,TextField,Textarea,Select,Button,ImageButton,HiddenField,CreateDiv,BidiLtr,BidiRtl,CreatePlaceholder,Flash,Smiley,PageBreak,Iframe,InsertPre,Font,FontSize,TextColor,BGColor,UIColor,Maximize,ShowBlocks,button1,button2,button3,oembed,MediaEmbed,About';
    config.format_tags = 'p;Heading';
    config.format_Heading = {element:"Heading", name: "Heading", styles:{ 'color': '#333333', 'display': 'block', 'font-size': '2.125em', 'font-weight': 'bold', 'font-family': 'Arial,Helvetica, sans-serif'}};
    
   
};


module ApplicationHelper

	def render_badge(type,text)
		"<span class='label label-#{type}'>#{text}</span>".html_safe
	end

	def resource_name
		:user
	end

	def resource
		@resource ||= User.new
	end

	def devise_mapping
		@devise_mapping ||= Devise.mappings[:user]
	end

	def calculate_tagging_url(request_url,new_tag)
		# A tagged url should contain /t/tag-1/tag-2/.../tag-n
		# If request_url contains /t/ append the new tag unless its already contained
		if request_url.include?('/tag')
			if request_url.include?(new_tag)
				# Remove the included tag
				tags = request_url.split('/tag/')[1].split('/')
				if tags.count == 1
					return request_url.split('/tag/')[0]
				else
					return request_url.gsub(new_tag,'').gsub('//','/')
				end
				return request_url
			else
				return "#{request_url}/#{new_tag}"
			end
		else
			return "#{request_url}/tag/#{new_tag}"
		end
	end
end

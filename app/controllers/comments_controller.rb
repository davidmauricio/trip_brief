class CommentsController < ApplicationController
  before_action :set_comment, only: [:show, :edit, :update, :destroy]
  before_action :set_tip, :except => [:create_comment_reply]
  before_action :authenticate_user!
  skip_before_action :verify_authenticity_token 

  def create_comment_reply
   comment_id = params[:comment_id]
   content = params[:content]
   CommentReplay.create(user: current_user, comment_id: comment_id, content: content)
   render json: {success: true, data: "create reply comment"}, status: :ok
  end
 
  # POST /comments
  # POST /comments.json
  def create
    @comment = current_user.comments.new(comment_params)
    @comment.tip = @tip

    respond_to do |format|
      if @comment.save
        format.html { redirect_to @comment.tip, notice: 'Comment was successfully created.' }
        format.json { render :show, status: :created, location: @comment }
      else
        format.html { render :new }
        format.json { render json: @comment.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /comments/1
  # PATCH/PUT /comments/1.json
  def update
    respond_to do |format|
      if @comment.update(comment_params)
        format.html { redirect_to @comment.tip, notice: 'Comment was successfully updated.' }
        format.json { render :show, status: :ok, location: @comment }
      else
        format.html { render :edit }
        format.json { render json: @comment.errors, status: :unprocessable_entity }
      end
    end
  end

  def vote_comment
      comment_id =  params[:comment_id]
      tip_id =  params[:tip_id]

      vote_comment = CommentVote.where(tip_id: tip_id ,user_id: current_user.id, comment_id: comment_id).first
      comment_votes = CommentVote.where(tip_id: tip_id, user_id: current_user.id, comment_id: comment_id).count
      
      #if vote_comment.nil?
          message = "crea"
          c = CommentVote.new(tip_id: tip_id, comment_id: comment_id, user_id: current_user.id)
          c.save
      #else            
          #vote_comment.destroy
          #message = "destruye"
      #end
      render json: {success: true, data: message}, status: :ok
  end

  def vote_comment_reply
      comment_id =  params[:comment_id]
      tip_id =  params[:tip_id]
      reply_id = params[:reply_id]

      vote_comment_reply = CommentReplyVote.where(tip_id: tip_id ,user_id: current_user.id, comment_id: comment_id, comment_replay_id: reply_id).first
      comment_reply_votes = CommentReplyVote.where(tip_id: tip_id, user_id: current_user.id, comment_id: comment_id, comment_replay_id: reply_id).count
      
        message = "crea"
        c = CommentReplyVote.new(tip_id: tip_id, comment_id: comment_id, user_id: current_user.id, comment_replay_id: reply_id)
        c.save
      
      render json: {success: true, data: message}, status: :ok
  end



  # DELETE /comments/1
  # DELETE /comments/1.json
  def destroy
    @comment.destroy
    respond_to do |format|
      format.html { redirect_to @tip, notice: 'Comment was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
  def set_tip
    @tip = Tip.find(params[:tip_id])
  end
    # Use callbacks to share common setup or constraints between actions.
    def set_comment
      @comment = Comment.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def comment_params
      params.require(:comment).permit(:user_id, :tip_id, :body)
    end
end

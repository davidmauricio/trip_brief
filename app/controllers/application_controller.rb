class ApplicationController < ActionController::Base
    protect_from_forgery with: :exception

    layout :layout_by_role

    before_action :configure_permitted_parameters, if: :devise_controller?

    def after_sign_up_path_for(resource_or_scope)
        edit_profile_path
    end

    protected

    def configure_permitted_parameters
        devise_parameter_sanitizer.permit(:sign_up, keys: [:username])
    end


    def layout_by_role
        if user_signed_in?
            if current_user.is_admin?
                "admin"
            else
                "application"
            end
        else
            "application"
        end
    end

    def redirect_by_role
        if current_user.is_super_admin?
            superadmin_path
        elsif current_user.is_school_admin?
            school_admin_path
        elsif current_user.is_parent?
            parent_path
        elsif current_user.is_teacher?
            teacher_path
        else
            root_path
        end
    end
end

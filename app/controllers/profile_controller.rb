class ProfileController < ApplicationController

    before_action :authenticate_user!
    
    layout 'application'

    def index
        @user = current_user
    end 
    
    def search
    end

    def show
        @user = current_user
    end

    def edit
        @user =  current_user
        
    end

    def update
        if current_user.update_attributes profile_params
            redirect_to my_profile_path, notice: "Your profile was updated successfully"
        else
            @user = current_user
            puts "-> #{@user.errors.full_messages.join(', ')}"
            flash.now[:notice] = "We couldn't save your profile"
            render 'edit'
        end
    end

    private
    def profile_params
        params.require(:user).permit(:avatar, :username, :birth_year, :gender, :bio, :recieve_email_updates, :external_link,  :email, country_ids: [])
    end

end
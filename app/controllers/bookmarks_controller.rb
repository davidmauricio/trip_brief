class BookmarksController < ApplicationController

	layout 'application'

	def index
		@user = current_user
		@tips = Tip.favorites_by current_user
	end
	
end

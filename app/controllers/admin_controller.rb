class AdminController < ApplicationController

    before_action :authenticate_user!
    before_action :only_admin
    layout 'application'

    def only_admin
        if user_signed_in?
            redirect_to root_path if !current_user.is_admin?
        else
            redirect_to root_path
        end
        
    end

end
class Admin::UsersController < AdminController

    layout 'admin'
    before_action :authenticate_user!

    def index
        @users = User.get_active.order('id ASC')
    end

    def new
        @user = User.new
    end

    def create
        @user = User.new user_params
        @user.password, @user.password_confirmation = @user.email
        if @user.save
            redirect_to admin_user_path(@user)
        else            
            render 'new'
        end
    end

    def edit
        @user = User.find params[:id]
    end

    def show
        @user = User.find params[:id]
    end

    def update
        @user = User.find(current_user.id)
        if @user.update(user_params)           
            redirect_to admin_user_path(@user)
        else            
            render 'edit'
        end
    end

    def destroy
        @user = User.find(current_user.id)
        # @user.inactivate # TODO: Defined inactivate actions
        redirect_to admin_users_path
    end
  
    private
    def user_params
        params.require(:user).permit!
    end
end
class Admin::TipsController < AdminController

    layout 'admin'
    before_action :authenticate_user!

    def index
        @tips = Tip.get_active.order('id DESC')
    end

    def edit  
        @tip = Tip.find params[:id]
    end

    def show
        @tip = Tip.find params[:id]
    end

    def update
        @tip = Tip.find params[:id]
        if @tip.update(tip_params)
            redirect_to admin_tip_path(@tip)
        else
            render 'edit'
        end
    end

    def destroy
        @tip = Tip.find params[:id]
        # @tip.inactivate # TODO: Defined inactivate actions
        redirect_to admin_tips_path
    end
  
    private
    def tip_params
        params.require(:tip).permit!
    end
end
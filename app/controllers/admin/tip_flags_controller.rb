class Admin::TipFlagsController < AdminController

    layout 'admin'
    before_action :authenticate_user!

    def index
        @flags = TipFlag.get_active.order('id DESC')
    end

    def show
        @flag = TipFlag.find params[:id]
    end

    def destroy
        @flag = Tip.find params[:id]
        # @tip.inactivate # TODO: Defined inactivate actions
        redirect_to admin_tips_path
    end
  
end
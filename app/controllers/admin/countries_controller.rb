class Admin::CountriesController < AdminController

    layout 'admin'
    before_action :authenticate_user!

    def index
        @countries = Country.get_active.order('id ASC')
    end

    def new
        @country = Country.new
    end

    def create
        @country = Country.new country_params
        
        if @country.save
            redirect_to admin_country_path(@country)
        else            
            render 'new'
        end
    end

    def edit  
        @country = Country.find params[:id]     
    end

    def show
        @country = Country.find params[:id]
    end

    def update
        @country = Country.find params[:id]
        if @country.update(country_params)           
            redirect_to admin_country_path(@country)
        else            
            render 'edit'
        end
    end

    def destroy
        @country = Country.find params[:id]
        # @country.inactivate # TODO: Defined inactivate actions
        redirect_to admin_countries_path
    end
  
    private
    def country_params
        params.require(:country).permit!
    end
end
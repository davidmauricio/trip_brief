class Admin::AttractionsController < AdminController

    layout 'admin'
    before_action :authenticate_user!

    def index
        @attractions = Attraction.get_active.order('id ASC')
    end

    def new
        @attraction = Attraction.new
    end

    def create
        @attraction = Attraction.new attraction_params
        
        if @attraction.save
            redirect_to admin_attraction_path(@attraction)
        else            
            render 'new'
        end
    end

    def edit  
        @attraction = Attraction.find params[:id]     
    end

    def show
        @attraction = Attraction.find params[:id]
    end

    def update
        @attraction = Attraction.find params[:id]
        if @attraction.update(attraction_params)           
            redirect_to admin_attraction_path(@attraction)
        else            
            render 'edit'
        end
    end

    def destroy
        @attraction = Attraction.find params[:id]
        # @attraction.inactivate # TODO: Defined inactivate actions
        redirect_to admin_attractions_path
    end
  
    private
    def attraction_params
        params.require(:attraction).permit!
    end
end
class Admin::CitiesController < AdminController

    layout 'admin'
    before_action :authenticate_user!

    def index
        @cities = City.get_active.order('id ASC')
    end

    def new
        @city = City.new
    end

    def create
        @city = City.new city_params
        
        if @city.save
            redirect_to admin_city_path(@city)
        else            
            render 'new'
        end
    end

    def edit  
        @city = City.find params[:id]     
    end

    def show
        @city = City.find params[:id]
    end

    def update
        @city = City.find params[:id]
        if @city.update(city_params)           
            redirect_to admin_city_path(@city)
        else            
            render 'edit'
        end
    end

    def destroy
        @city = City.find params[:id]
        # @city.inactivate # TODO: Defined inactivate actions
        redirect_to admin_cities_path
    end
  
    private
    def city_params
        params.require(:city).permit!
    end
end
class Admin::CategoriesController < AdminController

    layout 'admin'
    before_action :authenticate_user!

    def index
        @categories = Category.get_active.order('id ASC')
    end

    def new
        @category = Category.new
    end

    def create
        @category = Category.new category_params
        
        if @category.save
            redirect_to admin_category_path(@category)
        else            
            render 'new'
        end
    end

    def edit  
        @category = Category.find params[:id]     
    end

    def show
        @category = Category.find params[:id]
    end

    def update
        @category = Category.find params[:id]
        if @category.update(category_params)           
            redirect_to admin_category_path(@category)
        else            
            render 'edit'
        end
    end

    def destroy
        @category = Category.find params[:id]
        # @category.inactivate # TODO: Defined inactivate actions
        redirect_to admin_categories_path
    end
  
    private
    def category_params
        params.require(:category).permit!
    end
end
class DestinationsController < ApplicationController
    
    before_action :authenticate_user!, except: [:index, :show]
    layout 'application'

    def index
        if params[:q].present?
            query = params[:q]
            countries = Country.where('lower(name) like ? ', "%#{query}%").where(status: Country::Status[:active])
            cities = City.where('lower(name) like ?', "%#{query}%").where(status: City::Status[:active])
            attractions = Attraction.where("lower(name) like ?","%#{query}%").where(status: Attraction::Status[:active])

            @destinations = countries.map { |c| {name: c.full_name, id: c.id, type: 'country'} } + cities.map { |c| {name: c.full_name, id: c.id, type: 'city'} } + attractions.map { |c| {name: c.full_name, id: c.id, type: 'attraction'} }

        else
            countries = Country.all
            cities = City.all
            attractions = Attraction.all
            @destinations = countries.map { |c| {name: c.full_name, id: c.id, type: 'country'} } + cities.map { |c| {name: c.full_name, id: c.id, type: 'city'} } + attractions.map { |c| {name: c.full_name, id: c.id, type: 'attraction'} }
        end

        @destinations << {name: "See all results", id: params[:q], type: 'none'}
        
        respond_to do |format|
            format.html
            format.json{ 
                render json: @destinations, status: :ok
            }
        end
    end

    def search
        #.left_joins(:tip_votes).group("tips.id").order('COUNT(tip_votes.tip_id) DESC')
        @tips = Tip.joins(:categories).where("tips.status = #{Tip::Status[:active]} AND (tips.body like :q OR tips.subject like :q OR categories.name like :q)", q: "%#{params[:q]}%").uniq
        @tips = @tips.sort_by{|tip| tip.tip_votes.count }.reverse
    end  


    def create
        if params[:destination][:type] == Tip::EntityTypes[:country].to_s
            @destination = Country.new
        elsif params[:destination][:type] == Tip::EntityTypes[:city].to_s
            @destination = City.new
            @destination.country_id = params[:destination][:country_id]
        else
            @destination = Attraction.new
            if params[:destination][:country_id].present?
                @destination.country_id = params[:destination][:country_id]
            elsif params[:destination][:city_id].present?
                @destination.city_id = params[:destination][:city_id]
            else
                # Its a freeworld attraction
            end
        end
        @destination.name = params[:destination][:name]

        if @destination.save
            @response = {success: true}
        else
            @response = {success: false, message: @destination.errors.full_messages.join(', ')}
        end
    end

    def details
    end

    def show
        
        id = params[:id]
        type = params[:type]
        country_name_or_attraction_name = params[:country_name_or_attraction_name]
        city_or_attraction_name = params[:city_or_attraction_name]
        country_name = params[:country_name]
        city_name = params[:city_name]
        attraction_name = params[:attraction_name]

        if id.present? and !type.present?
            redirect_to root_path, notice: "Country or attraction not found" and return
        elsif id.present? and type.present?
            if type == 'country'
                @country = Country.find_by(id: id)
                if @country.nil?
                    redirect_to root_path, notice: "Country or attraction not found" and return 
                else
                    redirect_to country_or_attraction_path(country_name_or_attraction_name: @country.slug)
                end
            elsif type == 'city'
                @city = City.find_by(id: id)
                if @city.nil?
                    redirect_to root_path, notice: "Country or attraction not found" and return 
                else
                    redirect_to city_or_attraction_name_path(country_name: @city.country.slug, city_or_attraction_name: @city.slug)
                end
            elsif type == 'attraction'
                @attraction = Attraction.find_by(id: id)
                if @attraction.nil?
                    redirect_to root_path, notice: "Country or attraction not found" and return 
                else
                    if @attraction.city.present?
                        redirect_to attraction_name_path(country_name: @attraction.city.country.slug, city_name: @attraction.city.slug, attraction_name: @attraction.slug)
                    elsif @attraction.country.present?
                        redirect_to city_or_attraction_name_path(country_name: @attraction.country.slug, city_or_attraction_name: @attraction.slug)
                    else
                        redirect_to country_or_attraction_path(country_name_or_attraction_name: @attraction.slug)
                    end
                    
                end
            else
                redirect_to root_path, notice: "Country or attraction not found" and return
            end
        else
            if country_name_or_attraction_name.present?
                @country = Country.find_by(slug: country_name_or_attraction_name)
                if @country.nil?
                    @attraction = Attraction.find_by(slug: country_name_or_attraction_name)
                    if @attraction.nil?
                        redirect_to root_path, notice: "Country or attraction not found" and return
                    else
                        # Found attraction
                        @destination = @attraction
                        @bandera = false
                        @destination.tip_categories(params[:tags]).each do |category|
                           if request.url.include?(category[:slug]) 
                              @bandera = true
                           end
                        end 
                    end
                else
                    # Found country
                    @destination = @country
                    @bandera = false
                    @destination.tip_categories(params[:tags]).each do |category|
                       if request.url.include?(category[:slug]) 
                          @bandera = true
                       end
                    end 
                end
            elsif country_name.present? and city_or_attraction_name.present?
                @attraction = Attraction.find_by(slug: city_or_attraction_name)
                @city = City.find_by(slug: city_or_attraction_name)

                if @city.nil? and @attraction.nil?
                    redirect_to root_path, notice: "Country or attraction not found" and return
                elsif !@city.nil?
                    # Found city
                    @destination = @city
                    @bandera = false
                    @destination.tip_categories(params[:tags]).each do |category|
                       if request.url.include?(category[:slug]) 
                          @bandera = true
                       end
                    end 
                else
                    # Found attraction
                    @destination = @attraction
                    @bandera = false
                    @destination.tip_categories(params[:tags]).each do |category|
                       if request.url.include?(category[:slug]) 
                          @bandera = true
                       end
                    end 
                end
            elsif country_name.present? and city_name.present? and attraction_name.present?
                @attraction = Attraction.find_by(slug: attraction_name)

                if @attraction.nil?
                    redirect_to root_path, notice: "Country or attraction not found" and return
                else
                    @destination = @attraction
                    @bandera = false
                    @destination.tip_categories(params[:tags]).each do |category|
                       if request.url.include?(category[:slug]) 
                          @bandera = true
                       end
                    end 
                end
            else
                redirect_to root_path, notice: "Country or attraction not found" and return
            end
            
        end
       
    end

    def new_tip
        
    end


    def create_tip
        
    end

    private
    def tip_params
        params.require(:tip).permit(:subject, :body, :entity_id, :entity_type)
    end

    def destination_params
        params.require(:destination).permit(:name)
    end



end
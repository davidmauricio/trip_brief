class TipsController < ApplicationController

    before_action :authenticate_user!, only: [:tip, :vote, :edit, :show, :create, :new, :update ]
    before_action :set_tip, only: [:show, :edit, :update, :vote, :flag]
    skip_before_action :verify_authenticity_token  

    layout 'application'    
 
    def new
        @tip = Tip.new
        countries = Country.all
    end

    def detail
    end

    def create
        # Categories
        categories = []
        if params[:all_categories].present?
            params[:all_categories].each do |category|
                existing_category = Category.where(id: category).first
                if existing_category.nil?
                    new_category = Category.create(name: category)
                    categories.push new_category
                else
                    categories.push existing_category
                end
            end
        end

        # Tips. Dev note: # cant use tip_paras with new due to entity_id setting before entity_type
        @tip = Tip.new
        @tip.subject = params[:tip][:subject]
        @tip.body = params[:tip][:body]
        @tip.entity_type = params[:tip][:entity_type]
        @tip.entity_id = params[:tip][:entity_id]
        @tip.user_id = current_user.id
        @tip.categories = categories

        if @tip.save
            redirect_to @tip, notice: 'Tip created successfuly'
        else
            render 'new'
        end
    end

    def index
        id = params[:id]
        entity_type = params[:type]
        @tip = Tip.new(entity_id: id, entity_type: entity_type)
    end

    def show
        @tip = Tip.find params[:id]
        @comment = Comment.new
        @tip.update_visits_count
    end

    def edit
        @tip = Tip.find params[:id]
        if @tip.user.id != current_user.id
            redirect_to root_path
        end
    end

    def update
        # Categories
        categories = []
        if params[:all_categories].present?
            params[:all_categories].each do |category|
                existing_category = Category.where(id: category).first
                if existing_category.nil?
                    new_category = Category.create(name: category)
                    categories.push new_category
                else
                    categories.push existing_category
                end
            end
        end
        
        @tip = Tip.find params[:id]
        # Tips. Dev note: # cant use tip_paras with new due to entity_id setting before entity_type
        @tip.subject = params[:tip][:subject]
        @tip.body = params[:tip][:body]
        @tip.entity_type = params[:tip][:entity_type]
        @tip.entity_id = params[:tip][:entity_id]
        @tip.categories = categories

        if @tip.save
            redirect_to @tip, notice: 'Tip updated successfuly'
        else
            render 'edit'
        end
    end

    def vote_tip
        tip_id =  params[:tip_id]
        if user_signed_in?
            vote_tip = TipVote.create(tip_id: tip_id, user_id: current_user.id)
            success = true
        else
            success = false
        end
        
        render json: {success: success}, status: :ok
    end

    def create_flag_tip
        @tip_flag = TipFlag.new tip_flag_params
        if user_signed_in?
            @tip_flag.user_id = current_user.id
            if @tip_flag.save
                @response = {success: true}
            else
                @response = {success: false, message: @tip_flag.errors.full_messages.join(', ')}
            end
        else
            @response = {success: false, message: 'User not logged in'}
        end
        
    end

    def destination_search_autocomplete
        if params[:q].present?
            query = params[:q]
            countries = Country.where('lower(name) like ? ', "%#{query}%").where(status: Country::Status[:active])
            cities = City.where('lower(name) like ?', "%#{query}%").where(status: City::Status[:active])
            attractions = Attraction.where("lower(name) like ?","%#{query}%").where(status: Attraction::Status[:active])
            @destinations = countries.map { |c| {name: c.full_name, id: c.id, type: 'country'} } + cities.map { |c| {name: c.full_name, id: c.id, type: 'city'} } + attractions.map { |c| {name: c.full_name, id: c.id, type: 'attraction'} }

        else
            countries = Country.all
            cities = City.all
            attractions = Attraction.all
            @destinations = countries.map { |c| {name: c.full_name, id: c.id, type: 'country'} } + cities.map { |c| {name: c.full_name, id: c.id, type: 'city'} } + attractions.map { |c| {name: c.full_name, id: c.id, type: 'attraction'} }
        end
        
        respond_to do |format|
            format.html
            format.json{ 
                render json: @destinations, status: :ok
            }
        end
    end

    def favorite_tip
        tip_id =  params[:tip_id]
        favorite_tip = Favorite.where(tip_id: tip_id, user_id: current_user.id).first

        if favorite_tip.nil?
            message = "se creo"
            f = Favorite.new(tip_id: tip_id, user_id: current_user.id)
            f.save
        else
            favorite_tip.destroy
            message = "se destroy"
        end
        render json: {success: true, data: message}, status: :ok
    end

    def body
        @tip = Tip.find params[:id]
    end

    private

    def set_tip
        #@tip = Tip.find params[:id]
    end
    def tip_params
        params.require(:tip).permit(:subject, :body, :entity_id, :entity_type, :is_real_time, :month_applicable, :expiry_date, :country_id, :category_ids => [])
    end

    def tip_flag_params
        params.require(:tip_flag).permit(:option, :observation, :tip_id)
    end

end
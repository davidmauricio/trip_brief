class HomeController < ApplicationController

    #layout 'home'
    
    def index
    	@tips = Tip.where(status: Tip::Status[:active]).left_joins(:tip_votes).group(:id).order('COUNT(tip_votes.id) DESC').limit(10)
    	@tips_count = Tip.where(status: Tip::Status[:active]).count
    end

    def countries
    	render json: Country.all.map { |e| {id: e.id, name: e.name} }
    end

    def cities
    	render json: City.where(country_id: params[:country_id]).map { |e| {id: e.id, name: e.name} }
    end

end

class City < ApplicationRecord
    belongs_to :country

    default_scope { where(status: Status[:active]) }

    include Statusable
    include Destinatable
    
    validates :name, presence: true
    validates :country_id, presence: true

    has_many :tips, -> {where(status: Tip::Status[:active])}

    def full_name
        "#{self.name} - #{self.country.name}"
    end

    def entity_type
        'city'
    end
end

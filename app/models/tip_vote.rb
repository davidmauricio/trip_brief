class TipVote < ApplicationRecord

    belongs_to :tip
    belongs_to :user

    # validate :vote_limit

    def vote_limit    	
        limit = ENV['ALLOWED_VOTES_PER_USER']
        limit ||= 3
        errors.add(:user, 'You cant vote this tip anymore') if TipVote.where(tip_id: self.tip_id, user_id: self.user_id).count > limit
    end
   
end

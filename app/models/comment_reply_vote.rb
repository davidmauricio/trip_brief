class CommentReplyVote < ApplicationRecord

	belongs_to :user
 	belongs_to :tip
 	belongs_to :comment
 	belongs_to :comment_replay

end

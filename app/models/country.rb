class Country < ApplicationRecord

	include Statusable
    include Destinatable

    default_scope { where(status: Status[:active]) }

	has_many :cities 
    has_many :tips, -> {where(status: Tip::Status[:active])}

	validates :name, presence: true

    def full_name
        name
    end

    def entity_type
        'country'
    end
end

class Category < ApplicationRecord
    include Statusable

    validates :name, presence: true
    validates :slug, presence: false
    validates :name, uniqueness: true
    
    has_many :tip_categories
    has_many :tips, through: :tip_categories

    before_save :calculate_slug

    def calculate_slug
        if !self.slug.present?
            self.slug = self.name.downcase.gsub(' ','-').strip
        end
    end
end

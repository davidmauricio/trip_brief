class TipCategory < ApplicationRecord
    belongs_to :tip
    belongs_to :category
end

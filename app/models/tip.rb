class Tip < ApplicationRecord
    belongs_to :user

    belongs_to :country, optional: true
    belongs_to :city, optional: true
    belongs_to :attraction, optional: true

    has_many :tip_categories
    has_many :categories, through: :tip_categories
    has_many :tip_votes
    has_many :tip_flags
    has_many :comments
    has_many :favorite

    before_save :set_visits_count

    serialize :month_applicable, Array

    scope :favorites_by,  ->(user) {joins(:favorite).where(favorites: { user: user }) }

    include Statusable
    EntityTypes = {country: 0, city: 1, attraction: 2}
    Months = {january: 1, february: 2, march: 3, april: 4, may: 5, june: 6, july: 7, august:8, september: 9, october: 10, november: 11, december: 12}

    def to_param
        "#{id} #{subject}".parameterize
    end

    def entity_id=(value)
        puts "Coming value: #{value} - entity_type: #{entity_type_name}"
        if self.is_country_tip?
            self.country_id = value
            self.attraction_id = nil
            self.city_id = nil
        elsif self.is_city_tip?
            self.city_id = value
            self.attraction_id = nil
            self.country_id = nil
        elsif self.is_attraction_tip?
            self.attraction_id = value
            self.country_id = nil
            self.city_id = nil
        else
            errors.add(:entity_type, ' error. Please contact the administrator')
        end
    end

    def update_visits_count
        visits_count = self.visits_count
        visits_count = 0 if visits_count.nil?
        self.update(visits_count: visits_count + 1)
    end

    def is_country_tip?
        self.entity_type == EntityTypes[:country]
    end

    def is_city_tip?
        self.entity_type == EntityTypes[:city]
    end

    def is_attraction_tip?
        self.entity_type == EntityTypes[:attraction]
    end

    def entity_id
        if self.is_country_tip?
            return self.country_id
        elsif self.is_city_tip?
            return self.city_id
        elsif self.is_attraction_tip?
            return self.attraction_id
        else
            raise Exception.new("No entity found for this tip")
        end
    end

    def entity_type_name
        return "country" if self.entity_type == EntityTypes[:country]
        return "city" if self.entity_type == EntityTypes[:city]
        return "attraction" if self.entity_type == EntityTypes[:attraction]
    end

    def entity
        if self.is_country_tip?
            return self.country
        elsif self.is_city_tip?
            return self.city
        elsif self.is_attraction_tip?
            return self.attraction
        else
            return nil
        end
    end

    def country
        Country.where(id: self.entity_id).first
    end

    def city
        City.where(id: self.entity_id).first
    end

    def attraction
        Attraction.where(id: self.entity_id).first
    end

    def created_by_admin
        self.user.is_admin? ? "Yes" : "No"
    end

    def is_real_time_name
        self.is_real_time ? "Yes" : "No"
    end

    def self.entity_types_for_select
        {
            "Country" => EntityTypes[:country],
            "City" => EntityTypes[:city],
            "Attraction" => EntityTypes[:attraction]
        }
    end

    def self.by_category(category_id)
        Tip.joins(:categories).where(categories: {id: category_id, status: Category::Status[:active]}, status: Status[:active])
    end

    private

    def set_visits_count
        self.visits_count ||= 0
    end
end

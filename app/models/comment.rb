class Comment < ApplicationRecord
  belongs_to :user
  belongs_to :tip

  validates :body, presence: true
  has_many :comment_votes
  has_many :comment_replays 
end

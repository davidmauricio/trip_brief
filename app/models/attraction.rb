class Attraction < ApplicationRecord
    
    belongs_to :city, optional: true
    belongs_to :country, optional: true

    has_many :tips, -> {where(status: Tip::Status[:active])}

    include Statusable
    include Destinatable

    validates :name, presence: true

    def full_name
        if self.country.present? and self.city.present?
            return "#{self.name}, #{self.city.name} - #{self.city.country.name}"
        elsif self.country.present? and !self.city.present?
            return "#{self.name}, #{self.country.name}"
        elsif !self.country.present? and self.city.present?
            return "#{self.name}, #{self.city.name} - #{self.city.country.name}"
        else
            return "#{self.name}"
        end
    end

    def location_name
        if self.country.present? and self.city.present?
            return "#{self.city.name} - #{self.city.country.name}"
        elsif self.country.present? and !self.city.present?
            return "#{self.country.name}"
        elsif !self.country.present? and self.city.present?
            return "#{self.city.name} - #{self.city.country.name}"
        else
            return ""
        end
    end

    def has_location?
        self.city.present? or self.country.present?
    end

    def entity_type
        'attraction'
    end

end

class User < ApplicationRecord
	# Include default devise modules. Others available are:
	# :confirmable, :lockable, :timeoutable and :omniauthable
	devise :database_authenticatable, :registerable,
		:recoverable, :rememberable, :trackable, :validatable,
		:omniauthable, omniauth_providers: [:facebook]

	has_many :tips
	has_many :favorites
	has_many :comments

	has_many :user_countries, class_name: 'UserCountry'
	has_many :countries, through: :user_countries

	validates :username, presence: true, uniqueness: true
	
	include Statusable
	include Genderable

	Roles = {normal: 0, admin: 1}

	has_attached_file :avatar, 
		styles: { medium: "300x300>", thumb: "100x100>" }, 
		default_url: "/assets/profile.jpg"
	validates_attachment_content_type :avatar, content_type: /\Aimage\/.*\z/

	def is_admin?
		self.role == Roles[:admin]
	end

	def role_name
		return "Administrador" if self.role == Roles[:admin]
		return "Normal" if self.role == Roles[:normal]		
		return "NA"
	end

	def self.roles_for_select
		{
			"Administrador" => Roles[:admin],
			"Normal" => Roles[:normal]			
		}
	end

	def username_capitalized
		self.username.capitalize rescue ""
	end

	def countries_visited_count
		rand(1..70)
	end

	def self.from_omniauth(auth)
 		where(provider: auth.provider, uid: auth.uid).first_or_create do |user|
		    user.email = auth.info.email
		    user.password = Devise.friendly_token[0,20]
		    user.username = auth.info.name   
		    # user.image = auth.info.image 
		    
		end
	end

	def count_destinations
		Tip.select('attraction_id,city_id,country_id').where(user_id: self.id, status: Tip::Status[:active]).group('attraction_id,city_id,country_id').length
	end

	def count_tip_countries
		Tip.select('country_id').where(user_id: self.id, status: Tip::Status[:active]).group('country_id').length
	end
end

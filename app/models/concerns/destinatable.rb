module Destinatable
    extend ActiveSupport::Concern

    included do

        after_validation(on: :create) do
            if !self.slug.present?
                self.slug = self.name.gsub(' ','-').downcase
            end
        end
        
    end

    def tip_categories(tags=nil)
        if tags.present?
            tags = tags.split('/')
            self.tips_with_tags(tags).map { |t| t.categories.map { |c| {name: c.name.titleize, slug: c.slug, count: Tip.by_category(c.id).count} } }.flatten.uniq
        else
            self.tips.map { |e| e.categories.map { |c| {name: c.name.titleize, slug: c.slug, count: Tip.by_category(c.id).count} } }.flatten.uniq
        end
    end

    def tips_with_tags(tags)
        if tags.present?
            tags = tags.split('/')
            puts "-> Tags present: #{tags}"
            categories = Category.where(slug: tags)
            tips = self.tips.joins(:categories).where(categories:{id: categories})
            categories_ids = categories.map { |c| c.id }
            return self.tips.joins(:tip_votes).group("tip_votes.tip_id").order('COUNT(tip_votes.tip_id) DESC').select{|tip| (categories_ids - tip.category_ids).empty? }

        else
            puts "-> No tags present"
            return self.tips.joins(:tip_votes).group("tip_votes.tip_id").order('COUNT(tip_votes.tip_id) DESC')
        end
    end

end
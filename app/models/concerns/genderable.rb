module Genderable
    extend ActiveSupport::Concern

    included do
        Genders = {male: 1, female: 0}
    end

    def gender_name
        return "Man" if self.gender == Genders[:male]
        return "Woman " if self.gender == Genders[:female]
        return "NA"
    end

    module ClassMethods

        def gender_options
            {
                "Masculino" => Genders[:male],
                "Femenino" => Genders[:female]
            }
        end
        
    end

end
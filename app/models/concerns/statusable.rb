module Statusable
    extend ActiveSupport::Concern

    included do
        Status = {active: 1, inactive: 0, deleted: 2}
        validates :status, presence: true
    end

    def status_name
        return "Active" if self.status == Status[:active]
        return "Inactive" if self.status == Status[:inactive]
        return "Deleted" if self.status == Status[:deleted]
        return "NA"
    end

    def badge_color
        return "success" if self.status == Status[:active]
        return "default" if self.status == Status[:inactive]
        return "danger" if self.status == Status[:deleted]
        return "primary"
    end

    def activate
        self.update_attribute(:status, Status[:active])
    end

    def inactivate
        self.update_attribute(:status, Status[:inactive])
    end

    def delete
        self.update_attribute(:status, Status[:deleted])
    end

    def destroy
        raise Exception.new("Should not destroy any entity")
    end

    def is_active?
        self.status == Status[:active]
    end

    def is_inactive?
        self.status == Status[:inactive]
    end

    module ClassMethods

        def get_status_for_select
            {
                "Active" => Status[:active],
                "Inactive" => Status[:inactive],
                "Deleted" => Status[:deleted]
            }
        end

        def get_active
            where(status: Status[:active])
        end

    end

end
class TipFlag < ApplicationRecord
    belongs_to :tip
    belongs_to :user

    validate :unique_flag

    include Statusable

    def unique_flag
        errors.add(:user, ' has already flagged this tip') if TipFlag.where(user: self.user_id, tip_id: self.tip_id).any?
    end
end

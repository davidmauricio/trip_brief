class CommentReplay < ApplicationRecord
  belongs_to :user
  belongs_to :comment
  has_many :comment_reply_votes

  validates :content, presence: true
end